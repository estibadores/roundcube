FROM registry.sindominio.net/nginx

ARG UID=111
ARG GID=111
ARG PHP_VERSION=8.2

ENV SHA256 "c1b93a3edbe297457396b0a031d8b13c8a5dc30c9370704dfb9b2c1225017d52"
ENV ROUNDCUBEMAIL_VERSION 1.6.6

RUN apt-get update && \
    apt-get install -y --no-install-recommends \
    curl ca-certificates

RUN apt-get install -y --no-install-recommends \
    php${PHP_VERSION}-exif php${PHP_VERSION}-gd php${PHP_VERSION}-intl php${PHP_VERSION}-ldap php${PHP_VERSION}-mysql php${PHP_VERSION}-zip php${PHP_VERSION}-pspell  php${PHP_VERSION}-mbstring php${PHP_VERSION}-xml php${PHP_VERSION}-curl php${PHP_VERSION}-imagick php${PHP_VERSION}-sqlite3 \
    php${PHP_VERSION}-fpm \
    pwgen curl default-mysql-client

COPY conf/php.fpm.roundcube /etc/php/8.2/fpm/pool.d/www.conf
COPY conf/roundcube.nginx.conf /etc/nginx/sites-enabled/default

RUN chmod -R 777 /var/log && \
	chmod -R 777 /run/php/

RUN addgroup --gid ${GID} roundcube

RUN adduser \
   --system \
   --uid ${UID} \
   --gid ${GID} \
   --shell /usr/sbin/nologin \
   --gecos 'Roundcube' \
   --disabled-password \
   --home /roundcube \
   roundcube

WORKDIR /roundcube

COPY ./conf/composer.json composer.json

RUN chown roundcube:roundcube composer.json

USER roundcube

RUN curl -o roundcubemail.tar.gz -fSL https://github.com/roundcube/roundcubemail/releases/download/${ROUNDCUBEMAIL_VERSION}/roundcubemail-${ROUNDCUBEMAIL_VERSION}-complete.tar.gz; \
    echo "${SHA256} roundcubemail.tar.gz" > roundcubemail.tar.gz.asc && \
    sha256sum -c roundcubemail.tar.gz.asc && \
    tar xf roundcubemail.tar.gz -C . --strip-components=1 --no-same-owner && \
    rm -rf *.tar.gz *.asc;

## Config Plugins

# Plugins
RUN curl -s https://getcomposer.org/installer | php
RUN php composer.phar config --no-plugins allow-plugins.roundcube/plugin-installer true
RUN php composer.phar require "roundcube/plugin-installer"
RUN php composer.phar require "roundcube/larry:~1.6.0"
RUN php composer.phar require "roundcube/classic:~1.6.0"
RUN php composer.phar install

## ManageSieve
COPY ./conf/config.php.inc.managesieve plugins/managesieve/config.inc.php

## Customize Logo Sindominio
COPY ./conf/logo.svg skins/elastic/images/logo.svg
COPY ./conf/logo.png skins/classic/images/roundcube_logo.png
COPY ./conf/logo.png skins/larry/images/roundcube_logo.png

COPY entrypoint.sh /entrypoint.sh
COPY init-database.sh /init-database.sh
COPY conf/config.php.inc.env config/config.inc.php.sample

RUN chmod o+w config temp logs

ENTRYPOINT ["/bin/sh","/entrypoint.sh"]

ENV PHP_VERSION=${PHP_VERSION}
ENV PHP_USER=roundcube
ENV PHP_GROUP=roundcube

EXPOSE 80

ADD start.sh /start.sh
CMD ["/start.sh"]
