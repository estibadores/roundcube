# Roundcube

Roundcube Complete Version
https://roundcube.net/download/
Current: 1.5.3 stable
SHA-256: 4bcfac219f2e0005c912dac3227743cc1ed0ded69c822f74c81a70d041e5a3bd

Sindominio

## Configure

Copy .env.sample to .env

``` 
 cp .env.sample .env
```

Edit variables

``` 
 vim .env
``` 

Create volume directory

``` 
 mkdir data
 chown -R 1000:1000 data conf
``` 

## Build

Build a local image,
uncomment __build__ line and:

``` 
 docker-compose build
``` 

## Run

``` 
 docker-compose up -d
``` 

If first time, you need to initialize MySQL database

``` 
 docker-compose exec roundcubephp /bin/sh /init-database.sh
``` 

## Read PHP Logs

``` 
 docker-compose exec roundcubephp tail -f /tmp/php-fpm.log
``` 
