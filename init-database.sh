set -e

echo "Init Mysql Database"

cd /roundcubemail

mysql -h $RC_DB_HOST -u $RC_DB_USER -p$RC_DB_PASSWORD $RC_DB_NAME < SQL/mysql.initial.sql

exit 0

