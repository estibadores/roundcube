set -e;

configure() {

echo "Configure Roundcube"
cp -R config/config.inc.php.sample /tmp/config.inc.php
cd /tmp/

## Configure 
[ -n $RC_INSTALLER ] && sed -i "s:rcinstaller=.*:rcinstaller='$RC_INSTALLER';:" config.inc.php

## DATABASE
[ -n $RC_DB_TYPE ] && sed -i "s:dbtype=.*:dbtype='$RC_DB_TYPE';:" config.inc.php
[ -n $RC_DB_HOST ] && sed -i "s:dbserver=.*:dbserver='$RC_DB_HOST';:" config.inc.php
[ -n $RC_DB_USER ] && sed -i "s:dbuser=.*:dbuser='$RC_DB_USER';:" config.inc.php
[ -n $RC_DB_NAME ] && sed -i "s:dbname=.*:dbname='$RC_DB_NAME';:" config.inc.php
[ -n $RC_DB_PASSWORD ] && sed -i "s:dbpass=.*:dbpass='$RC_DB_PASSWORD';:" config.inc.php

## IMAP
[ -n $RC_DEFAULT_HOST ] && sed -i "s#imaphost=.*#imaphost='$RC_DEFAULT_HOST';#" config.inc.php
[ -n $RC_DEFAULT_PORT ] && sed -i "s:imapport=.*:imapport='$RC_DEFAULT_PORT';:" config.inc.php
## SMTP
[ -n $RC_SMTP_SERVER ] && sed -i "s#smtphost=.*#smtphost='$RC_SMTP_SERVER';#" config.inc.php
[ -n $RC_SMTP_PORT ] && sed -i "s:smtpport=.*:smtpport='$RC_SMTP_PORT';:" config.inc.php
## OPTIONS
[ -n $RC_SUPPORT_URL ] && sed -i "s#rcsupporturl=.*#rcsupporturl='$RC_SUPPORT_URL';#" config.inc.php
[ -n $RC_PRODUCT_NAME ] && sed -i "s#rcproductname=.*#rcproductname='$RC_PRODUCT_NAME';#" config.inc.php
[ -n $RC_MAIL_DOMAIN ] && sed -i "s#rcmaildomain=.*#rcmaildomain='$RC_MAIL_DOMAIN';#" config.inc.php
[ -n $RC_SKIN ] && sed -i "s:rcskin=.*:rcskin='$RC_SKIN';:" config.inc.php

RC_DES_KEY=$(pwgen -Bs1 24)
[ -n $RC_DES_KEY ] && sed -i "s:rcdeskey=.*:rcdeskey='$RC_DES_KEY';:" config.inc.php

cp config.inc.php /roundcube/config/config.inc.php
chmod 440 /roundcube/config/config.inc.php
rm config.inc.php

cd /roundcube/

}

init() {
	## INSTALL INITAL TABLES
	mysql -h $RC_DB_HOST -u $RC_DB_USER -p$RC_DB_PASSWORD $RC_DB_NAME < SQL/mysql.initial.sql; exit
}

touch config/config.inc.php
[ ! -s config/config.inc.php ] && configure

echo "Start Roundcube"
exec $@
